﻿using System;
using System.Threading;

namespace BankSystemDelegates
{
    class Program
    {
        private const int THREAD_COUNT = 10;

        public static void Main(string[] args)
        {
            var bankAccount = new BankAccount
            {
                FullName = "Some person"
            };

            var reporter = new ConsoleReporter();
            var reporterDelegate = new ReporterDelegate(reporter.SendReport);

            bankAccount.ReportEvent += reporterDelegate;
            
            var timer = new Timer(FinancialTransfer, bankAccount, TimeSpan.FromSeconds(0), TimeSpan.FromSeconds(60));
            
            Console.ReadLine();
            timer.Dispose();
        }

        private static void FinancialTransfer(object account)
        {
            var bankAccount = (BankAccount)account;

            var threads = new Thread[THREAD_COUNT];
            
            for (int i = 0; i < threads.Length; i++)
            {
                if (i % 2 == 0)
                {
                    var thread = new Thread(bankAccount.Add);
                    threads[i] = thread;
                }
                else
                {
                    var thread = new Thread(bankAccount.Withdraw);
                    threads[i] = thread;
                }
            }

            foreach (var thread in threads)
            {
                thread.Start(200);
            }
        }
    }
}
