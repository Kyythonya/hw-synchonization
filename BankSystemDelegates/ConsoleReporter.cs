﻿using System;

namespace BankSystemDelegates
{
    public class ConsoleReporter: IReporter
    {
        public void SendReport(string message)
        {
            Console.WriteLine(message);
        }
    }
}
